# IDS 721 Mini Proj 4 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-4/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-4/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 4 - Containerize a Rust Actix Web Service**

## Goal
* Containerize simple Rust Actix web app
* Build Docker image
* Run container locally

## App & Docker Screenshots
* My web app has a simple function to generate a random lucky number
![Screenshot_2024-02-20_at_9.13.52_PM](/uploads/5f3bc61cd16819bc53011bbb5c6b8aeb/Screenshot_2024-02-20_at_9.13.52_PM.png)
* My Docker Image
![Screenshot_2024-02-20_at_9.16.12_PM](/uploads/0c3d5f99082c4f9d8d3d2d52f72b32cf/Screenshot_2024-02-20_at_9.16.12_PM.png)
* My Docker Container
![Screenshot_2024-02-20_at_9.16.49_PM](/uploads/53e453d9d060895eea70d8f7c243de60/Screenshot_2024-02-20_at_9.16.49_PM.png)

## Key Steps
1. Run `cargo new <project_name>` to create a new Rust project
2. Create `cargo.toml` and add the following dependencies:
```
actix-web = "4"
rand = "0.8.4"
actix-files = "0.6.0"
handlebars = "4"
serde = { version = "1", features = ["derive"] }
log = "0.4"
env_logger = "0.9"
serde_json = "1.0"
```
3. Build your web app in `src/main.rs`
4. Build your UI in `src/index.html`
5. Run `cargo build` to build the finished project
6. Run `cargo run` to test and run the web app
7. Create a `Dockerfile` and copy my code there
8. Run `docker build -t <image_name> .` to build the Docker image
9. Run `docker run -p 8080:8080 <image_name>` to run the Docker container
10. Navigate to `localhost:8080` to test the web app

## Reference
* [Actix Documentation](https://actix.rs/docs/getting-started)