use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde_json::json; // Import the json macro

// Endpoint to generate lucky number
async fn generate_lucky_number() -> impl Responder {
    let lucky_number = rand::thread_rng().gen_range(1..=100);
    HttpResponse::Ok().json(json!({ "lucky_number": lucky_number }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(web::resource("/generate_lucky_number").route(web::get().to(generate_lucky_number)))
            .route("/", web::get().to(index))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}

// Serve the HTML Page
async fn index() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(include_str!("index.html"))
}
